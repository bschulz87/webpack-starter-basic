/* eslint-disable no-console */
"use strict";

(function main(window, document) {
    checkConfirmation();
    addRequestParamsAndSubmitTrackingToForms();

    [].slice.call(document.querySelectorAll('button[data-tracking-param]')).forEach(function(el) {
        var param = el.getAttribute('data-tracking-param');
        el.addEventListener('click', function () {
            var form = document.getElementById('form-overlay');
            var input = form.querySelector('input[name=retURL]');
            input.setAttribute('value','https://erfolgreich-werben.xing.com/danke?sc_o=' + param);

            fadeOverlayIn(form);
        });
    });

    document.getElementById('form-overlay-close').addEventListener('click', function() {
        fadeOverlayOut(document.getElementById('form-overlay'));
    });

    document.getElementById('confirmation-overlay-close').addEventListener('click', function() {
        fadeOverlayOut(document.getElementById('confirmation-overlay'));
    });

    document.getElementById('form-overlay').addEventListener('click', function(e) {
        e.target.id == 'form-overlay' ? fadeOverlayOut(this) : null;
    });

    document.getElementById('confirmation-overlay').addEventListener('click', function(e) {
        e.target.id == 'confirmation-overlay' ? fadeOverlayOut(this) : null;
    });

    function checkConfirmation() {

        var params = window.location.search.substring(1).split("&");
        params.forEach(function(param) {
            var pair = param.split('=');
            if(pair[0] === 's' && pair[1] === '1') {

                /* Fire Google conversion */
                window.google_trackConversion({
                    google_conversion_id: 931485209,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "rDBYCKqw92oQmayVvAM",
                    google_remarketing_only: false
                });

                /* Fire Adfarm conversion */
                var iFrame = document.createElement('iframe');
                iFrame.src = 'https://ad2.adfarm1.adition.com/track?tid=12426&sid=34586&type=html&orderid=&itemno=&descr=&quantity=&price=0.00&total=0.00';
                iFrame.setAttribute('allowtransparency', 'true');
                iFrame.setAttribute('scrolling', 'no');
                iFrame.setAttribute('border', '0');
                iFrame.setAttribute('width', '1');
                iFrame.setAttribute('height', '1');
                iFrame.setAttribute('marginwidth', '0');
                iFrame.setAttribute('marginheight', '0');
                iFrame.setAttribute('background-color', 'transparent');

                document.body.appendChild(iFrame);

                return fadeOverlayIn(document.getElementById('confirmation-overlay'));
            }
        });
    }

    function addRequestParamsAndSubmitTrackingToForms() {
        var params = window.location.search.substring(1).split("&");

        if(params[0].length > 0) {
            var param = params[0].split('=');

            if(param[0] === 'rac' || param[0] === 'rce') {
                var input = document.createElement("input");
                input.setAttribute('type', 'hidden');
                input.setAttribute('name', '00Nb0000009jmom');
                input.setAttribute('value', params[0]);

                [].slice.call(document.forms).forEach(function (form) {
                    form.appendChild(input.cloneNode(false));
                });
            }
        }
    }

    function fadeOverlayIn(el) {
        el.style.clip = 'auto';
        el.style.width = '100%';
        el.style.height = '100%';
        el.style.margin = '0';
        el.style.position = 'fixed';
        el.style.opacity = '1';
    }

    function fadeOverlayOut(el) {
        el.style.opacity = '0';
        setTimeout(function() {
                el.style.clip = 'rect(0 0 0 0)';
                el.style.width = '1px';
                el.style.height = '1px';
                el.style.margin = '-1px';
                el.style.position = 'absolute';
            }, 400
        );
    }
}(window, document));
