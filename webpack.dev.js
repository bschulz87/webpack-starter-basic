const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    devtool: 'eval-cheap-module-source-map',
    entry: './src/index.js',
    devServer: {
        port: 8080,
        contentBase: path.join(__dirname, "dist")
    },
    resolve: {
        extensions: ['.less', '.pug', '.html', '.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['es2015']
                }
            },

            {
                test: /\.pug$/,
                loader: 'pug-loader'
            },

            /*
            {
              test: /\.html$/,
              loader: 'html-minify-loader',
              options: {
                quotes: true,
                dom: { lowerCaseTags: false }
              }
            },
            */
            /*
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: "sass-loader",
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: true,
                            sourceMapContents: true
                        }
                    },
                    // Please note we are not running postcss here
                ]
            },
            */
            {
                test: /\.(less|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                    },
                    {
                        // compiles less to CSS
                        loader: "less-loader",
                    }
                    // Please note we are not running postcss here
                ]
            },
            { test: /\.svg(\?.*)?$/,   loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml" },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[path][name].[ext]?hash=[hash:20]',
                            limit: 8192
                        }
                    }
                ]
            }
        ],
    },
    plugins: [
        //new ExtractTextPlugin("[name].html"),
        new HtmlWebpackPlugin({
            title: 'XING Marketing Analytics 2018',
            template: './src/pages/index.pug',
            hash: true,
            inject: true
        })
        /*
        ,
        new HtmlWebpackPlugin({
            title: 'Danke - XING Marketing Analytics 2018',
            //template: './index.html',
            filename: 'danke/index.html',
            hash: true,
            inject: true
        })
        */
    ]
};
