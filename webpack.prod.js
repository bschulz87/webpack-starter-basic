const path = require('path');

// const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const HtmlCriticalPlugin = require("html-critical-webpack-plugin");

const buildPath = path.resolve(__dirname, 'dist')

module.exports = {
    devtool: 'source-map',
    entry: './src/index.js',
    output: {
        filename: '[name].[hash:20].js',
        path: buildPath
    },
    resolve: {
        extensions: ['.less', '.pug', '.html', '.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                options: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.(less|css)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            // translates CSS into CommonJS
                            loader: 'css-loader'
                        },
                        {
                            // Runs compiled CSS through postcss for vendor prefixing
                            loader: 'postcss-loader'
                        },
                        {
                            // compiles Sass to CSS
                            loader: 'less-loader'
                        }
                    ],
                    fallback: 'style-loader'
                }),
            },
            /*
            {
                test: /\.(scss|css)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            // translates CSS into CommonJS
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // Runs compiled CSS through postcss for vendor prefixing
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // compiles Sass to CSS
                            loader: 'sass-loader',
                            options: {
                                outputStyle: 'expanded',
                                sourceMap: true,
                                sourceMapContents: true
                            }
                        }
                    ],
                    fallback: 'style-loader'
                }),
            },
            */
            {
                test: /\.pug$/,
                loader: 'pug-loader'
            },
            { test: /\.svg(\?.*)?$/,   loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml" },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[hash:20].[ext]',
                            limit: 8192
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(buildPath),
        new UglifyJSPlugin(),
        new ExtractTextPlugin('styles.[contentHash].css', {
            allChunks: true
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                map: {
                    inline: false,
                },
                discardComments: {
                    removeAll: true
                }
            },
            canPrint: true
        }),
        new HtmlWebpackPlugin({
            title: 'XING Marketing Analytics 2018',
            template: './src/pages/index.pug',
            hash: true,
            inject: true
        }),
        new HtmlWebpackPlugin({
            title: 'Danke - XING Marketing Analytics 2018',
            template: './src/pages/index.pug',
            filename: '/danke/index.html',
            hash: true,
            inject: true
        }),
        new HtmlCriticalPlugin({
          base: path.join(path.resolve(__dirname), 'dist/'),
          src: 'index.html',
          dest: 'index.html',
          inline: true,
          minify: true,
          extract: true,
          penthouse: {
            blockJSRequests: false,
          }
        })
        /*
        new HtmlCriticalPlugin({
          base: path.join(path.resolve(__dirname), 'dist/danke/'),
          src: 'index.html',
          dest: 'index.html',
          inline: true,
          minify: true,
          extract: true,
          penthouse: {
            blockJSRequests: false,
          }
        })
        */
        /*
        new CriticalPlugin({
          src: '/danke/index.html',
          inline: true,
          minify: true,
          dest: '/danke/index.html'
        }),
        */
    ]
};
